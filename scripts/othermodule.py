"""
	Returns sum of numbers
"""
import functools

def factorize_numbers( *numbers ):
	return functools.reduce( lambda x,y: x*y, numbers)